

export function addDevices(DeviceManager: any): void {

        // Desktop
        DeviceManager.add('Desktop', '');

        // Mobile
        DeviceManager.add('Mobile', '320px', {
            widthMedia: '480px', // the width that will be used for the CSS media
        });
    }
