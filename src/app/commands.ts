export function addCommands(editor) {

    ///////////// EXPORT
    editor.on('run:export-template:before', opts => {
        console.log('Before the command run');
        if (0 /* some condition */) {
            opts.abort = 1;
        }
    });
    editor.on('run:export-template', () => console.log('After the command run'));
    editor.on('abort:export-template', () => console.log('Command aborted'));


    /////////////// DEVICES
    // Device Desktop
    editor.Commands.add('set-device-desktop', {
        run: editor => editor.setDevice('Desktop')
    });
    //Device Mobile
    editor.Commands.add('set-device-mobile', {
        run: editor => editor.setDevice('Mobile')
    });
    // Device changed
    editor.on('change:device', () => console.log('Current device: ', editor.getDevice()));


    /////////// PANELS
    // Show layers
    editor.Commands.add('show-layers', {
        getRowEl(editor) { return editor.getContainer().closest('.editor-row'); },
        getLayersEl(row) { return row.querySelector('.layers-container') },

        run(editor, sender) {
            const lmEl = this.getLayersEl(this.getRowEl(editor));
            lmEl.style.display = '';
        },
        stop(editor, sender) {
            const lmEl = this.getLayersEl(this.getRowEl(editor));
            lmEl.style.display = 'none';
        },
    });

    // Show styles
    editor.Commands.add('show-styles', {
        getRowEl(editor) { return editor.getContainer().closest('.editor-row'); },
        getStyleEl(row) { return row.querySelector('.styles-container'); },

        run(editor, sender) {
            const smEl = this.getStyleEl(this.getRowEl(editor));
            smEl.style.display = '';
        },
        stop(editor, sender) {
            const smEl = this.getStyleEl(this.getRowEl(editor));
            smEl.style.display = 'none';
        },
    });

    // Show traits
    editor.Commands.add('show-traits', {
        getTraitsEl(editor) {
            const row = editor.getContainer().closest('.editor-row');
            return row.querySelector('.traits-container');
        },
        run(editor, sender) {
            this.getTraitsEl(editor).style.display = '';
        },
        stop(editor, sender) {
            this.getTraitsEl(editor).style.display = 'none';
        },
    });

    // Show blocks
    editor.Commands.add('show-blocks', {
        getRowEl(editor) { return editor.getContainer().closest('.editor-row'); },
        getBlockEl(row) { return row.querySelector('.blocks-container'); },

        run(editor, sender) {
            const smEl = this.getBlockEl(this.getRowEl(editor));
            smEl.style.display = '';
        },
        stop(editor, sender) {
            const smEl = this.getBlockEl(this.getRowEl(editor));
            smEl.style.display = 'none';
        },
    });

    editor.Commands.add('my-command-id', {
        run(editor) {
            alert('This is my command');
        },
    });
}
