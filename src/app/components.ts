export function addComponents(DomComponents) {
    const defaultType = DomComponents.getType('default');
    const defaultModel = defaultType.model;
    const defaultView = defaultType.view;

    const inputTypes = [
        { value: 'text', name: 'Text' },
        { value: 'email', name: 'Email' },
        { value: 'password', name: 'Password' },
        { value: 'number', name: 'Number' },
    ];

    // The `input` will be the Component type ID
    DomComponents.addType('input', {
        // Define the Model
        model: defaultModel.extend({
            // Extend default properties
            defaults: Object.assign({}, defaultModel.prototype.defaults, {
                // Can't drop other elements inside it
                droppable: false,
                // Traits (Settings)
                traits: ['name', 'placeholder', {
                    // Change the type of the input (text, password, email, etc.)
                    type: 'select',
                    label: 'Type',
                    name: 'type',
                    options: inputTypes,
                }, {
                        // Can make it required for the form
                        type: 'checkbox',
                        label: 'Required',
                        name: 'required',
                    }],
            }),
        },
            // The second argument of .extend are static methods and we'll put inside our
            // isComponent() method. As you're putting a new Component type on top of the stack,
            // not declaring isComponent() might probably break stuff, especially if you extend
            // the default one.
            {
                isComponent(el) {
                    if (el.tagName === 'INPUT') {
                        return { type: 'input' };
                    }
                },
            }),

        // Define the View
        view: defaultType.view.extend({
            // Bind events
            events: {
                // If you want to bind the event to children elements
                // 'click .someChildrenClass': 'methodName',
                click: 'handleClick',
                dblclick() {
                }
            },

            // It doesn't make too much sense this method inside the component
            // but it's ok as an example
            randomHex() {
                return '#' + Math.floor(Math.random() * 16777216).toString(16);
            },

            handleClick(e) {
                this.model.set('style', { color: this.randomHex() }); // <- Affects the final HTML code
                this.el.style.backgroundColor = this.randomHex(); // <- Doesn't affect the final HTML code
                // Tip: updating the model will reflect the changes to the view, so, in this case,
                // if you put the model change after the DOM one this will override the backgroundColor
                // change made before
            },

            // The render() should return 'this'
            render() {
                // Extend the original render method
                defaultType.view.prototype.render.apply(this, arguments);
                this.el.placeholder = 'Text here'; // <- Doesn't affect the final HTML code
                return this;
            },
        }),
    });



    // The `button` will be the Component type ID
    DomComponents.addType('button', {
        // Define the Model
        model: defaultModel.extend({
            // Extend default properties
            defaults: Object.assign({}, defaultModel.prototype.defaults, {
                // Can't drop other elements inside it
                droppable: false,
                // Traits (Settings)
                traits: ['name', 'placeholder', {
                    // Change the type of the input (text, password, email, etc.)
                    type: 'select',
                    label: 'Type',
                    name: 'type',
                    options: inputTypes,
                }, {
                        // Can make it required for the form
                        type: 'checkbox',
                        label: 'Required',
                        name: 'required',
                    }],
            }),
        },
            // The second argument of .extend are static methods and we'll put inside our
            // isComponent() method. As you're putting a new Component type on top of the stack,
            // not declaring isComponent() might probably break stuff, especially if you extend
            // the default one.
            {
                isComponent(el) {
                    if (el.tagName === 'aaaa') {
                        return { type: 'aaa' };
                    }
                },
            }),

        // Define the View
        view: defaultType.view.extend({
            // Bind events
            events: {
                // If you want to bind the event to children elements
                // 'click .someChildrenClass': 'methodName',
                click: 'handleClick',
                dblclick() {
                    console.log('double click on the button!!!');
                }
            },

            // It doesn't make too much sense this method inside the component
            // but it's ok as an example

            handleClick(e) {
                console.log('click on a button');
            },

            // The render() should return 'this'
            render() {
                // Extend the original render method
                defaultType.view.prototype.render.apply(this, arguments);
                this.el.placeholder = 'Text here'; // <- Doesn't affect the final HTML code
                return this;
            },
        }),
    });
}
