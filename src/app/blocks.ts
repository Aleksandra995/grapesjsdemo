export function addBlocks(BlockManager) {
  // Section
  BlockManager.add('section', {
    label: '<b>Section</b>', // You can use HTML/SVG inside labels
    attributes: { class: 'gjs-block-section' },
    content: `<section>
            <h1>This is a simple title</h1>
            <div>This is just a Lorem text: Lorem ipsum dolor sit amet</div>
          </section>`,
  });

  // Text
  BlockManager.add('text', {
    label: 'Text',
    content: '<div data-gjs-type="text">Insert your text here</div>',
  });

  // Image
  BlockManager.add('image', {
    label: 'Image',
    select: true,
    content: { type: 'image' },
    activate: true,
  });

  // Input
  BlockManager.add('input', {
    label: 'Input',
    // select: true,
    name: 'input',
    content: {
      type: 'input', content: '<input />',
      style: {
        width: '60px',
        height: '25px',
        color: 'white',
        'font-size': '14px',
        'font-weight': '300',
        'background-color': 'darkOrange',
        'text-align': 'center',
        'line-height': '25px',
      }
    },
  });

  // Blabla
  BlockManager.add('my-block-id', {
    label: 'blabla',
    content: '<div data-gjs-type="text">blabla</div>',
  });

  // Button
  BlockManager.add('button', {
    label: 'Button',
    name: 'button',
    category: '',
    //content: '<h1>Put your title here</h1>',
    content: {
      type: 'button',
      script: function klik() {
        console.log('fsfgsfsr');
      },
      // Add some style just to make the component visible
      content: '<button class="button">Button</button>',

      attributes: { class: 'gjs-fonts gjs-f-button' }
    }
  });

  BlockManager.add('some-block-id', {
    label: `<div>
            <img src="https://picsum.photos/70/70"/>
            <div class="my-label-block">Label block</div>
          </div>`,
    content: '<div>...</div>',
    render: ({ el }) => {
      const btn = document.createElement('button');
      btn.innerHTML = 'Click me';
      btn.addEventListener('click', () => alert('Do something'))
      el.appendChild(btn);
    },
  });
}
