import { Component, OnInit } from '@angular/core';
import { addDevices } from './devices';
import { addCommands } from './commands';
import { addBlocks } from './blocks';
import { addStyleSectors } from './style_sectors';
import { addPanels } from './panels';
import { addAssets } from './assets';
import { addComponents } from './components';

declare var grapesjs: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  ngOnInit() {
    const editor = grapesjs.init({
      container: '#gjs',
      fromElement: true,
      // Size of the editor
      height: '300px',
      width: 'auto',
      // Avoid any default panel
      panels: {
        defaults: [{}]
      },
      // Setting remote storage
      storageManager: {
        type: 'remote',
        stepsBeforeSave: 2,
        urlStore: 'http://192.168.0.241:5000/api/website',
        urlLoad: 'http://192.168.0.241:5000/api/website',
        params: {}, // Custom parameters to pass with the remote storage request, eg. CSRF token
        headers: {}, // Custom headers for the remote storage request
      },
      layerManager: {
        appendTo: '.layers-container'
      },
      selectorManager: {
        appendTo: '.styles-container'
      },
      styleManager: {
        appendTo: '.styles-container',
      },
      traitManager: {
        appendTo: '.traits-container',
      },
      blockManager: {
        appendTo: '.blocks-container',
      }
    });

    addDevices(editor.DeviceManager);

    addPanels(editor.Panels);

    addStyleSectors(editor.StyleManager);

    addBlocks(editor.BlockManager);

    addAssets(editor.AssetManager);

    addCommands(editor);

    addComponents(editor.DomComponents);
  }
}
